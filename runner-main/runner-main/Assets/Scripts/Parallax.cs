﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    #region Datos
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;
    public static bool activar = true;
    #endregion

    void Start()
    {
        activar = true;
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        //Activa el Parallax
        if (activar == true)
        {
            transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);
            if (transform.localPosition.x < -20)
            {
                transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
            }
        }

        //Desactiva el Parallax
        if (activar == false)
        {
            transform.position = new Vector3(transform.position.x - parallaxEffect * 0, transform.position.y, transform.position.z);
        }
    }
}
