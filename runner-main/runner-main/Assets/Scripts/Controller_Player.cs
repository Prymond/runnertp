﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    #region Datos
    private Rigidbody rb;

    [SerializeField] private float jumpForce;
    [SerializeField] private float minX;
    [SerializeField] private float maxX;
    [SerializeField] private float movForce;

    private float initialSize;
    private int i = 0;
    private bool floored;
    public int vidas;
    #endregion

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
    }

    void Update()
    {

        GetInput();
        Muerte();
    }

    private void GetInput()
    {
        MovHorizontal();
        Limitar();
        Jump();
        Duck();
    }

    #region Movimientos
    private void Jump()
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void MovHorizontal()
    {
        if (floored)
        {
            float movHorizontal = Input.GetAxis("Horizontal") * movForce;
            movHorizontal *= Time.deltaTime;
            transform.Translate(movHorizontal, 0, 0);
        }
    }

    private void Limitar()
    {
        Vector3 limites = transform.position;
        limites.x = Mathf.Clamp(limites.x, minX, maxX);
        transform.position = limites;
    }

    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    public void Muerte()
    {
        if (vidas <= 0)
        {
            Destroy(this.gameObject); Controller_Hud.gameOver = true;
        }
    }

#endregion

    #region Colliders
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            vidas -= 1;
        }



        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }
    #endregion
}
